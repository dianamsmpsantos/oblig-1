import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.LinkedList;

public class Teque {

    private LinkedList<Integer> frontDeque;
    private LinkedList<Integer> rearDeque;

    public Teque() {
        frontDeque = new LinkedList<Integer>();
        rearDeque = new LinkedList<Integer>();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Teque teque = new Teque();
        teque.read_input(scanner);
    }

    public void push_back(int e) {
        if (isEmpty()) {
            frontDeque.addLast(e);
        } else if (isEven()) {
            
            rearDeque.addLast(e);
            int rearFirst = rearDeque.removeFirst();
            frontDeque.addLast(rearFirst);
        } else {
            rearDeque.addLast(e);
        }  
    }

    public void push_front(int e) {
        if (isEmpty()) {
            frontDeque.addLast(e);
        } else if (isEven()) {
            frontDeque.addFirst(e);
        } else {
            frontDeque.addFirst(e);
            int frontLast = frontDeque.removeLast();
            rearDeque.addFirst(frontLast);
        }
    }

    public void push_middle(int e) {
        if (isEmpty()) {
            frontDeque.addLast(e);
        } else if (isEven()) {
            frontDeque.addLast(e);
        } else {
            rearDeque.addFirst(e);
        }
    }

    public void get(int i) {
        // printer det i-te elementet i køen.
        if (i < frontDeque.size()) {
            int e = frontDeque.get(i);
            System.out.println(e);
        } else {
            int e = rearDeque.get(i - frontDeque.size());
            System.out.println(e);
        }
    }

    private int size() {
        return frontDeque.size() + rearDeque.size();
    }

    private boolean isEmpty() {
        return size() == 0;
    }

    private boolean isEven() {
        return frontDeque.size() == rearDeque.size();
    }

    public void read_input(Scanner scanner) {
        BufferedReader bi = new BufferedReader(new InputStreamReader(System.in));
        String line;
        try {
            while ((line = bi.readLine()) != null) {
                String[] linesplit = line.split(" ");
                String command;
                int number;

                if (linesplit.length > 1) {
                    command = linesplit[0];
                    number = Integer.valueOf(linesplit[1]);
    
                    if (command.equals("push_back")) {
                        push_back(number);
                    } else if (command.equals("push_front")) {
                        push_front(number);
                    } else if (command.equals("push_middle")) {
                        push_middle(number);
                    } else if (command.equals("get")) {
                        get(number);
                    }
                }
            }
        } catch (IOException e) {
            
        }

        scanner.close();
    } 
}